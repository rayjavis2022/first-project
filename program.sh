#!/bin/bash

greeting(){
hour=$(date +"%H")
    if [ $hour -ge 0 -a $hour -lt 12 ]
    then
        greet="Good Morning, $1"
    elif [ $hour -ge 12 -a $hour -lt 18 ] 
    then
        greet="Good Afternoon, $1"
    else 
        greet="Good Evening, $1"
    fi
    echo $greet
    echo "UPTIME:" $(uptime | sed 's/.*up \([^,]*\), .*/\1/') 
    echo "DATE: "$(date)
}
signature(){
echo  "
    _/_/_/_/_/  _/_/_/    _/_/_/  _/_/_/_/_/
       _/      _/    _/    _/          _/   
      _/      _/_/_/      _/        _/      
     _/      _/    _/    _/      _/         
    _/      _/    _/  _/_/_/  _/_/_/_/_/  

                        Copyrights 2022®    
    " 


}

loadingScreen(){
    echo -ne 'Loading:                     ( 0%)\r';
    sleep 2
    echo -ne 'Loading:####                 (33%)\r';
    sleep 2
    echo -ne 'Loading:###########          (66%)\r';
    sleep 2
    echo -ne 'Loading:####################(100%)\r';
    sleep 2
    echo -ne '                                  \r';


}

memoryStats(){
    
    echo "
        ####---MEMORY STATISTICS---####
        "
    echo $(grep MemTotal /proc/meminfo | awk '{printf("Total memory: %.2f GB"), $2/1048576}')
    echo $(free -m | awk '/^Mem/ {printf("Used Memory: %.2f GB", $3/1024);}')
    echo $(free -m | awk '/^Mem/ {printf("Memory Usage: %u%%", 100*$3/$2);}')
    m=$(free -m | awk '/^Mem/ {printf("%i", 100*$3/$2);}')

    # if conditions are used to determine the state of memory consumption
    if (( $m >= 0 && $m < 40 ))
    then
        s="Low memory consumption"
    elif (( $m >= 40 && $m < 70 )) 
    then
        s="Moderate memory consumption"
    elif (( $m >= 70 && $m < 95 )) 
    then
        s="High memory consumption"
    else
        s="Memory consumption is very high - Please reduce the memory usage"
    fi

    echo "Memory Usage status:" $s
}

OSdetails(){
    echo "
        ####---OPERATING SYSTEM DETAILS---####
    "
    echo "Operating System Name:" $(cat /etc/os-release | grep -w "NAME" | awk -F "=" '{print $2}')
    echo "Operating System version:" $(cat /etc/os-release | grep -w "PRETTY_NAME" | awk -F "=" '{print $2}' | awk -F " " '{print $2} ')

}
signature
# loadingScreen
greeting $USER
memoryStats
OSdetails


